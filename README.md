Make U Installed with postgresql 10

Установка комплекта клиент/сервер постреляционной субд и конфигурирование стандартных файлов с возможностью варьирования количества подключений, общего буфера и максим$

Для запуска вводится строка:
ansible-playbook pg_10_install.yml -i webservers.yml -Kk

Prerequisites
Для запуска используется ОС семейства "Красная Шляпа" CentOS 7.

Installing
ansible-playbook pg_10_install.yml -i webservers.yml -Kk

Проверка:
1.  sudo -i
2.  (ввод пароля)
3.  su postgres
4.  psql
5.  Вы находитесь в СУБД

Versioning
1.0.0.431

Authors
s.vavilov

Acknowledgments

gitlab.com

docs.ansible.com

stackoverflow.com

...
